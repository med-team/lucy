Source: lucy
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/med-team/lucy
Vcs-Git: https://salsa.debian.org/med-team/lucy.git
Homepage: https://lucy.sourceforge.net
Rules-Requires-Root: no

Package: lucy
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: DNA sequence quality and vector trimming tool
 Lucy is a utility that prepares raw DNA sequence fragments for sequence
 assembly, possibly using the TIGR Assembler. The cleanup process includes
 quality assessment, confidence reassurance, vector trimming and vector
 removal. The primary advantage of Lucy over other similar utilities is
 that it is a fully integrated, stand alone program.
 .
 Lucy was designed and written at The Institute for Genomic Research
 (TIGR, now the J. Craig Venter Institute), and it has been used here for
 several years to clean sequence data from automated DNA sequencers prior
 to sequence assembly and other downstream uses. The quality trimming
 portion of lucy makes use of phred quality scores, such as those produced
 by many automated sequencers based on the Sanger sequencing method. As
 such, lucy’s quality trimming may not be appropriate for sequence
 data produced by some of the new “next-generation” sequencers.
