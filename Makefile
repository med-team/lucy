CC = cc
CFLAGS = -O
LIBS = -lpthread
OBJS = lucy.o qual_trim.o abi.o vector.o splice.o poly.o

all: lucy beep

lucy: $(OBJS)
	rm -f $@
	$(CC) $(CFLAGS) -o $@ $(OBJS) $(LIBS)

beep:
	@echo done!

clean:
	rm -f $(OBJS) lucy


