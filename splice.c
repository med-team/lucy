/****************************************************************************
*
* Copyright (c) 2003, The Institute for Genomic Research (TIGR), Rockville,
* Maryland, U.S.A.  All rights reserved.
*
****************************************************************************/

#define MAXIMUM_THREADS 32

#define NIL 0
#define LFT 1
#define DIA 2
#define TOP 3

#define Score(x, y) splice_score_base[(y)*width+(x)]
#define Direc(x, y) splice_direc_base[(y)*width+(x)]

int  *splice_match_base;
int  *splice_score_pool[MAXIMUM_THREADS+1];
char *splice_direc_pool[MAXIMUM_THREADS+1];

static int match[16][16]={
/*       A   C   G   T   U   R   Y   M   W   S   K   D   H   V   B   N */
/*A*/ {  5,-14,-14,-14,-14,  2,-14,  2,  2,-14,-14,  1,  1,  1,-14,  0,},
/*C*/ {-14,  5,-14,-14,-14,-14,  2,  2,-14,  2,-14,-14,  1,  1,  1,  0,},
/*G*/ {-14,-14,  5,-14,-14,  2,-14,-14,-14,  2,  2,  1,-14,  1,  1,  0,},
/*T*/ {-14,-14,-14,  5,  5,-14,  2,-14,  2,-14,  2,  1,  1,-14,  1,  0,},
/*U*/ {-14,-14,-14,  5,  5,-14,  2,-14,  2,-14,  2,  1,  1,-14,  1,  0,},
/*R*/ {  2,-14,  2,-14,-14,  2,-14,  1,  1,  1,  1,  1,  0,  1,  0,  0,},
/*Y*/ {-14,  2,-14,  2,  2,-14,  2,  1,  1,  1,  1,  0,  1,  0,  1,  0,},
/*M*/ {  2,  2,-14,-14,-14,  1,  1,  2,  1,  1,-14,  0,  1,  1,  0,  0,},
/*W*/ {  2,-14,-14,  2,  2,  1,  1,  1,  2,-14,  1,  1,  1,  0,  0,  0,},
/*S*/ {-14,  2,  2,-14,-14,  1,  1,  1,-14,  2,  1,  0,  0,  1,  1,  0,},
/*K*/ {-14,-14,  2,  2,  2,  1,  1,-14,  1,  1,  2,  1,  0,  0,  1,  0,},
/*D*/ {  1,-14,  1,  1,  1,  1,  0,  0,  1,  0,  1,  1,  0,  0,  0,  0,},
/*H*/ {  1,  1,-14,  1,  1,  0,  1,  1,  1,  0,  0,  0,  1,  0,  0,  0,},
/*V*/ {  1,  1,  1,-14,-14,  1,  0,  1,  0,  1,  0,  0,  0,  1,  0,  0,},
/*B*/ {-14,  1,  1,  1,  1,  0,  1,  0,  0,  1,  1,  0,  0,  0,  1,  0,},
/*N*/ {  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,},
/*       A   C   G   T   U   R   Y   M   W   S   K   D   H   V   B   N */
};

#define GAPOPEN -17
#define GAPGROW -5

static int left_penalty[]={ GAPOPEN, GAPGROW, GAPOPEN, GAPOPEN };
static int  top_penalty[]={ GAPOPEN, GAPOPEN, GAPOPEN, GAPGROW };

void splice_align_left(id, a, width, b, height, offset, good, right_end)
char *a, *b;
int id, width, height, offset, good, *right_end;
{
  register left, top, direc, score, i, j;
  register int  *splice_score_base;
  register char *splice_direc_base;
  int max_score, max_i, max_j;

  splice_score_base=splice_score_pool[id];
  splice_direc_base=splice_direc_pool[id];
  
  *right_end=-1;
  if (width<2 || height<2)
    return;

  while (1) { /* keep trimming until no more vector found */
    
    /* setting the upper-left-most cell */
    Score(0,0)=max_score=match[*a][*b];
    Direc(0,0)=NIL;
    max_i=max_j=0;
    
    /* setting the first row */
    for (i=1; i<width; i++) {
      direc=NIL;
      score=match[a[i]][*b];
      left=Score(i-1,0)+left_penalty[Direc(i-1,0)];
      if (score<left) {
	direc=LFT; score=left;
      }
      Direc(i,0)=direc;
      Score(i,0)=score;
      if (score>max_score) {
	max_score=score; max_i=i; max_j=0;
      }
    }
  
    /* setting the first column */
    for (j=1; j<height; j++) {
      direc=NIL;
      score=match[*a][b[j]];
      top=Score(0,j-1)+top_penalty[Direc(0,j-1)];
      if (score<top) {
	direc=TOP; score=top;
      }
      Direc(0,j)=direc;
      Score(0,j)=score;
      if (score>max_score) {
	max_score=score; max_i=0; max_j=j;
      }
    }
  
    /* running dynamic programming for internal nodes */
    for (j=1; j<height; j++)
      for (i=1; i<width; i++) {
	score=Score(i-1,j-1);
	if (score>=0) {
	  score+=match[a[i]][b[j]];
	  direc=DIA;
	} else {
	  score=match[a[i]][b[j]];
	  direc=NIL;
	}
	top=Score(i,j-1)+top_penalty[Direc(i,j-1)];
	left=Score(i-1,j)+left_penalty[Direc(i-1,j)];
	if (top>left) {
	  if (score<top) {
	    direc=TOP; score=top;
	  }
	} else if (left>top) {
	  if (score<left) {
	    direc=LFT; score=left;
	  }
	} else if (score<top) {
	  if (i*height>=j*width) {
	    direc=LFT; score=left;
	  } else {
	    direc=TOP; score=top;
	  }
	}
	Direc(i,j)=direc;
	Score(i,j)=score;
	if (score>=max_score) {
	  max_score=score; max_i=i; max_j=j;
	}
      }
    
    /* bracktrack to find the longest alignment path */
    i=max_i; j=max_j;
    while (direc=Direc(i,j))
      switch (direc) {
      case LFT:
	i--; break;
      case DIA:
	i--; j--; break;
      case TOP:
	j--; break;
      }

    /* check if done or continue chopping */
    j=offset+max_i;
    left=max_i-i+1;
    if (left<splice_match_base[j]) {
      if (j<splice_match_base[j])
	*right_end=j;
      return;
    } else if (left<i+offset-good && left<width/2)
      return;
    else {
      *right_end=j;
      max_i++; max_j++;
      offset+=max_i;
      a+=max_i;
      width-=max_i;
      b+=max_j;
      height-=max_j;
      if (width<splice_match_base[j] || height<splice_match_base[j])
	return;
    }
  }
}

void splice_align_right(id, a, width, b, height, offset, span, left_end)
char *a, *b;
int id, width, height, offset, span, *left_end;
{
  register left, top, direc, score, i, j;
  int max_score, max_i, max_j;
  register int  *splice_score_base;
  register char *splice_direc_base;

  splice_score_base=splice_score_pool[id];
  splice_direc_base=splice_direc_pool[id];
  
  *left_end=-1;
  if (width<span || height<span)
    return;

  /* setting the upper-left-most cell */
  Score(0,0)=max_score=match[*a][*b];
  Direc(0,0)=NIL;
  max_i=max_j=0;

  /* setting the first row */
  for (i=1; i<width; i++) {
    direc=NIL;
    score=match[a[i]][*b];
    left=Score(i-1,0)+left_penalty[Direc(i-1,0)];
    if (score<left) {
      direc=LFT; score=left;
    }
    Direc(i,0)=direc;
    Score(i,0)=score;
    if (score>max_score) {
      max_score=score; max_i=i; max_j=0;
    }
  }
  
  /* setting the first column */
  for (j=1; j<height; j++) {
    direc=NIL;
    score=match[*a][b[j]];
    top=Score(0,j-1)+top_penalty[Direc(0,j-1)];
    if (score<top) {
      direc=TOP; score=top;
    }
    Direc(0,j)=direc;
    Score(0,j)=score;
    if (score>max_score) {
      max_score=score; max_i=0; max_j=j;
    }
  }
  
  /* running dynamic programming for internal nodes */
  for (j=1; j<height; j++)
    for (i=1; i<width; i++) {
      score=Score(i-1,j-1);
      if (score>=0) {
	score+=match[a[i]][b[j]];
	direc=DIA;
      } else {
	score=match[a[i]][b[j]];
	direc=NIL;
      }
      top=Score(i,j-1)+top_penalty[Direc(i,j-1)];
      left=Score(i-1,j)+left_penalty[Direc(i-1,j)];
      if (top>left) {
	if (score<top) {
	  direc=TOP; score=top;
	}
      }	else if (left>top) {
	if (score<left) {
	  direc=LFT; score=left;
	}
      } else if (score<top) {
	if (i*height>=j*width) {
	  direc=LFT; score=left;
	} else {
	  direc=TOP; score=top;
	}
      }
      Direc(i,j)=direc;
      Score(i,j)=score;
      if (score>=max_score) {
	max_score=score; max_i=i; max_j=j;
      }
    }

  /* bracktrack to find the longest alignment path */
  i=max_i; j=max_j;
  while (direc=Direc(i,j))
    switch (direc) {
    case LFT:
      i--; break;
    case DIA:
      i--; j--; break;
    case TOP:
      j--; break;
    }

  j=max_i-i+1;
  
  /* The length of a good hit must meet one of these 2 criteria: */
  /*   at least as long as the minimum good hit (span), and at least */
  /*   as long as the remaining target sequence that follows the hit; */
  /*   or longer than half the size of the splice sequence. */
  if (j>=span && j>=width-max_i || j>height/2)
    *left_end=i+offset; 
}
